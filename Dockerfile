FROM ubuntu
RUN apt-get update && apt-get install -yyq \
	curl wget \
	&& rm -rf /var/lib/apt/lists/*

ARG EDITION=community
ARG VERSION=1.13.1

#ADD https://downloads.mongodb.com/compass/mongodb-compass-${EDITION}_${VERSION}_amd64.deb mongodb-compass-${EDITION}_${VERSION}_amd64.deb
RUN wget https://downloads.mongodb.com/compass/mongodb-compass-${EDITION}_${VERSION}_amd64.deb
RUN apt-get update && apt-get install -yyq \
	./mongodb-compass-${EDITION}_${VERSION}_amd64.deb \
	libgtk2.0 libx11-xcb1 libxtst6 libxss1 libnss3 libasound2 \
	&& rm -rf /var/lib/apt/lists/*
RUN rm mongodb-compass-${EDITION}_${VERSION}_amd64.deb
CMD mongodb-compass-community
